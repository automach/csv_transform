require 'minitest/autorun'
require_relative './lib'

require 'stringio'

class PrintLineTest < Minitest::Test
  def test_basic
    assert_equal 'a,1', capture_print_line(['a', 1])
  end

  def test_commas
    assert_equal '"a, b"', capture_print_line(['a, b'])
  end

  def test_double_quotes
    assert_equal '"a""b",""""', capture_print_line(['a"b', '"'])
  end

  def test_new_lines
    assert_equal %("a\nb","c\rd"), capture_print_line(["a\nb", "c\rd"])
  end

  private

  def capture_print_line(values)
    io = StringIO.new
    print_line(values, io)
    io.rewind
    io.string.strip
  end
end

class ParseDurationTest < Minitest::Test
  def test_basic
    assert_equal 3661.001, parse_duration('01:01:01.001')
  end

  def test_without_padding
    assert_equal 3661.001, parse_duration('1:1:1.001')
  end

  def test_bad_syntax
    assert_raises(InvalidDurationSyntax) { parse_duration('0101:01.001') }
  end

  def test_utf_replacement
    assert_raises(InvalidDurationSyntax) { parse_duration('01:0�:01.001') }
  end
end

class ParseTimestampTest < Minitest::Test
  def test_basic
    eastern = ActiveSupport::TimeZone.new("US/Eastern")
    assert_equal eastern.local(2011, 1, 1, 3, 0, 1), parse_timestamp('1/1/11 12:00:01 AM')
  end

  def test_invalid_input
    assert_raises(TimestampParseFailure) { parse_timestamp('1/1/11 12:00:01') }
  end

  def test_invalid_input
    assert_raises(TimestampParseFailure) { parse_timestamp('1/1/11 12:00:0� AM') }
  end
end

class ReadCSVTest < Minitest::Test
  def test_basic
    input = StringIO.new('a,b,c')
    read_csv(input) do |values|
      assert_equal %w(a b c), values
    end
  end

  def test_quotes_values
    input = StringIO.new('a,b,"c,d"')
    read_csv(input) do |values|
      assert_equal %w(a b c,d), values
    end
  end

  def test_bad_unicode
    input = StringIO.new("a,b,\255c")
    read_csv(input) do |values|
      assert_equal %w(a b �c), values
    end
  end

  def test_strips_last_element
    input = StringIO.new("a\r\n")
    read_csv(input) do |values|
      assert_equal %w(a), values
    end
  end

  def test_handles_escaped_quotes
    input = StringIO.new('"a""b",c')
    read_csv(input) do |values|
      assert_equal %w(a"b c), values
    end
  end
end
