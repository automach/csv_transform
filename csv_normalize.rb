require_relative './lib'

def clean_values(values)
  timestamp, address, zip, fullname, foo_duration, bar_duration, _, notes = values

  fd_in_secs = parse_duration(foo_duration)
  bd_in_secs = parse_duration(bar_duration)

  [
    parse_timestamp(timestamp).iso8601,
    address,
    zip.to_s.rjust(5, '0'),
    unicode_aware_upcase(fullname),
    fd_in_secs,
    bd_in_secs,
    (fd_in_secs + bd_in_secs).round(3),
    notes
  ]
end

@line_number = 0
def process_values(values)
  values_to_print = @line_number == 0 ? values : clean_values(values)
  print_line(values_to_print, STDOUT)
rescue TimestampParseFailure, InvalidDurationSyntax => e
  STDERR.puts "Error parsing line #{@line_number}: #{e.message}"
ensure
  @line_number += 1
end

read_csv(STDIN) do |values|
  process_values(values)
end
