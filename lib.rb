require 'active_support/time'

# Print each value within `values` to `io`, quoting any that include a comma,
# quote, or newline. Double quotes will be replaced with two double quotes to
# escape them.
def print_line(values, io)
  values.each_with_index do |value, i|
    stringified = value.to_s
    if [',', '"', "\n", "\r"].any? { |c| stringified.include?(c) }
      io.print %("#{stringified.gsub('"', '""')}")
    else
      io.print stringified
    end
    io.print ',' unless i == values.size - 1
  end
  io.print("\r\n")
end

class InvalidDurationSyntax < StandardError; end

# Parses HH:MM::SS.ms duration into a floating point number of seconds
def parse_duration(string)
  unless string =~ /\d{1,2}:\d{1,2}:\d{1,2}\.\d{3}/
    raise InvalidDurationSyntax, "#'{string}' is not in valid HH:MM:SS.MS format"
  end
  hours, minutes, seconds, milliseconds = string.split(/:|\./).map(&:to_i)
  sum = milliseconds / 1000.0
  sum += seconds
  sum += minutes * 60
  sum += hours * 60 * 60
  sum
end

class TimestampParseFailure < StandardError; end

# Parse datetime in Pacific time and return it in Eastern. Daylight savings
# should be handled automatically.
def parse_timestamp(timestamp)
  pacific = ActiveSupport::TimeZone.new("US/Pacific")
  begin
    time = pacific.strptime(timestamp, "%m/%d/%y %l:%M:%S %p")
  rescue ArgumentError
    # raise a custom error type for upstream detection. ArgumentError is too
    # vague to be useful.
    raise TimestampParseFailure, "'#{timestamp}' could not be parsed"
  end
  eastern = ActiveSupport::TimeZone.new("US/Eastern")
  time.in_time_zone(eastern)
end

# Ruby's built-in upcase does not handle all unicode characters properly
def unicode_aware_upcase(string)
  ActiveSupport::Multibyte::Chars.new(string).upcase
end

# Read lines iteratively from `input`, yielding an array of values for each
# line parsed. Converts input to UTF-8, replacing invalid characters with �.
def read_csv(input)
  loop do
    line = input.readline
    line.encode!("UTF-8", invalid: :replace, undef: :replace)

    values = ['']
    within_quote = false
    index = 0

    loop do
      c = line[index]
      break unless c

      if c == '"'
        if line[index + 1] == '"'
          index += 1 # skip next double quote
          values.last << '"'
        else
          within_quote = !within_quote
        end
      elsif c == ',' && !within_quote
        values << ''
      else
        values.last << c
      end
      index += 1
    end
    values.last.strip!

    yield values
  end
rescue EOFError
end
