# Setup

1. Make sure you are running Ruby 2.4.2. This project was developed on OS X.
2. Run `bundle install` to install dependencies. If you don't have Bundler
   installed, first run `gem install bundler`.

# Running

Process CSVs over standard in by piping them to the script: `cat
some-data.csv | bundle exec ruby csv_transform.rb`. Output will be written
to standard out, and warnings will go to standard error.

# Tests

After completing the setup above, you can run the tests using `bundle exec ruby
lib_test.rb`.
